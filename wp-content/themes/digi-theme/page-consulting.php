<?php 
/**
* Template Name: Consulting
*
* @package WordPress
* @subpackage Digilite Theme
*/

get_header(); ?>
<section class="page-info">
	<div class="container">
		<h1 class="page-g-title"><?php the_title(); ?></h1>
		<div class="cons-in-page-desc">
			<?php $topimage = get_field("cons_top_image");
				  $fullimage = get_field("cons_full_image"); ?>
			<img src="<?= $topimage; ?>" alt="" title="">
			<div class="cons-page-info-t">
				<p class="info-title title-bold"><?php the_field("cons_info_title"); ?></p>
				<p class="info-desc"><?php the_field("cons_info_desc"); ?></p>
			</div>
		</div>
	</div>
</section>
<section class="cons-image">
	<p>Available March 2021</p>
	<img src="<?= $fullimage; ?>" alt="" title="">
</section>
<section class="consulting text-center">
	<div class="container">
	    <h5 class="block-name light-name">Get consulting</h5>
		<h3 class="s-title cons_bk"><?php the_field("cons_title", 2); ?></h3>
		<a href="<?php the_field("cons_url", 2); ?>" class="book btn-b"><?php the_field("cons_button", 2); ?></a>
	</div>
</section>
<?php get_footer(); ?>