<?php 
/**
* Template Name: Buzz & Jazz
*
* @package WordPress
* @subpackage Digilite Theme
*/ ?>

<?php get_header(); ?>
<div class="single-post-content">
	<div class="right_block">
		<div class="flex-container image_buzz">
			<div class="buzz">
				<img src="../wp-content/themes/digi-theme/img/dance.jpg" alt="" title="">
				<h1 class="page-g-title">BUZZ & JAZZ</h1>
			</div>
		</div>
	</div>
	<div class="container">
	   <div class="row">
	       <div class="col-md-4 col-sm-12 boy-image">
		    	<img src="../wp-content/themes/digi-theme/img/boy.png" alt="" title=""> 
		   </div>
		   <div class="thumb">
				<div class="blog-item">
					<div class="">
						<?php echo get_the_post_thumbnail( get_the_ID(), 'blog-thumb', array("class"=>"img-fluid") ); ?>
					</div>
					<h1><?php the_title(); ?></h1>
				</div>
		   </div>
		   <div class="ps-content post<?= $count; ?>">
					<?php the_content(); ?>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>