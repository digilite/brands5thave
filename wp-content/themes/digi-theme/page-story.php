<?php 
/**
* Template Name: Our Story
*
* @package WordPress
* @subpackage Digilite Theme
*/

get_header(); ?>
<section class="page-info">
	<div class="container">
		<h1 class="page-g-title"><?php the_field("story_page_title"); ?></h1>
		<div class="in-page-desc">
			<p class="info-title"><?php the_field("story_info_title"); ?></p>
			<?php the_field("story_info_description"); ?>
		</div>
	</div>
</section>
<section class="story-image">
	<div class="container">
		<?php $image = get_field("story_image"); ?>
		<image src="<?= $image; ?>" alt="" title="">
	</div>
</section>
<?php get_footer(); ?>