<?php 
/**
* Template Name: Brands
*
* @package WordPress
* @subpackage Digilite Theme
*/

get_header(); ?>
<section class="page-info">
	<div class="container">
		<?php $image1 = get_field("br_g_image"); ?>
		<h1 class="page-g-title"><?php the_field("br_title"); ?></h1>
		<img class="brand-g-image" src="<?= $image1; ?>" alt="" title="">
		<h3 class="brand-title">Body Sprays</h3>
		<div class="br-in-page-desc">
			<p><?php the_field("br_description"); ?></p>
			<h3><?php the_field("br_second_title"); ?></h3>
		</div>
	</div>
</section>
<section class="br-image">
	<div class="container owl-theme owl-carousel" id="owl-demo">
	<?php

// Check rows exists.
if( have_rows('br_second_slide') ):

    // Loop through rows.
    while( have_rows('br_second_slide') ) : the_row();

        // Load sub field value.
        $slide = get_sub_field('sc_slide_image'); ?>
        <div class="item"><img src="<?= $slide; ?>" alt=""></div>
   <?php // End loop.
    endwhile;

// No value.
else :
    // Do something...
endif; ?>
	</div>
</section>
<?php get_footer(); ?>