<?php 
/**
* Template Name: Licensing
*
* @package WordPress
* @subpackage Digilite Theme
*/

get_header(); ?>
<section class="page-info">
	<div class="container">
		<h1 class="page-g-title"><?php the_field("lc_page_title"); ?></h1>
		<div class="lc-in-page-desc">
			<?php the_field("lc_description"); ?>
		</div>
	</div>
</section>
<section class="lc-image story-image">
	<div class="container">
		<?php $image = get_field("lc_image"); ?>
		<image src="<?= $image; ?>" alt="" title="">
	</div>
</section>
<?php get_footer(); ?>