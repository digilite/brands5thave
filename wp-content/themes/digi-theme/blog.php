<?php 
/**
* Template Name: Blog
*
* @package WordPress
* @subpackage Digilite Theme
*/

get_header(); ?>
<section class="blog">
	<div class="container text-center">
	<h5 class="block-name light-name">Brands fifth ave</h5>
	<h3 class="s-title"><?php the_field("sc_title"); ?></h3>
		<div class="post-container flex-container">
				<?php
				if ( get_query_var('paged') ) { $paged = get_query_var('paged'); } else if ( get_query_var('page') ) {$paged = get_query_var('page'); } else {$paged = 1; }
				$args = array(
					'post_type'         => 'post',
					'post_status'       => 'publish',
					'paged'             => $paged,
					'posts_per_page'    => 3
				);
				$temp = $wp_query;
				$wp_query= null;
				$wp_query = new WP_Query($args);
				$count = 1;
				while ($wp_query -> have_posts()) : $wp_query -> the_post();
				$count++;
				?>
				<div class="blog-item col-md-4 col-sm-12">
					<div class="thumb text-center">
						<?php echo get_the_post_thumbnail( get_the_ID(), 'blog-thumb', array("class"=>"img-fluid") ); ?>
					</div>
					<p class="post-author"><?php the_author();?></p>
					<a class="post-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				</div>
				<?php 
				endwhile;
				the_posts_pagination();
				$wp_query = null;
				$wp_query = $temp;
				wp_reset_query();
			?>
    	</div>
	</div>
</section>
<?php get_footer(); ?>