<?php get_header(); ?>
<div class="page-content">
    <div class="container">
    <h1 class="page-g-title">Our <span>Story</span></h1>
    <?php

    // Check value exists.
    if( have_rows('inner_page_content') ):

        // Loop through rows.
        while ( have_rows('inner_page_content') ) : the_row();

            // Case: Paragraph layout.
            if( get_row_layout() == 'page_info_text' ):
                $description = get_sub_field('inner_page_info_desc'); ?>
                <div class="in-page-desc">
                <?= $description; ?>
                </div>
        <?php  
                // Do something...

            endif;

        // End loop.
        endwhile;

    // No value.
    else :
        // Do something...
    endif; ?>
    </div>
</div>
<?php get_footer(); ?>