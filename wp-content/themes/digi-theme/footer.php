		<footer itemscope itemtype="http://schema.org/WPFooter">
		    <div class="container">
				<div class="flex-container footer-info">
					<div class="col-md-3 col-sm-12">
					<p class="footer-title">Menu</p>
					<?php wp_nav_menu([
						'theme_location' => 'primary-menu',
						'menu_class' => 'footer-menu',
						'container' => '',
				    ]); ?>
					</div>
					<div class="col-md-3 col-sm-12 contact-info">
						<p class="footer-title">Address</p>
						<div class="ft-adress">
							<i class="fas fa-map-marker-alt"></i>
							<p>9225 Leslie Street,<br>Suite 201 | Richmond Hill,<br>Ontario |L4B 3H6 | Canada</p>
						</div>
						<div class="ft-phone">
							<i class="fab fa-whatsapp"></i>
							<p>(289) 234-1788</p>
						</div>
						<div class="ft-mail">
							<i class="fas fa-envelope"></i>
							<p>info@brandsfifthave.com</p>
						</div>
					</div>
					<div class="col-md-3 col-sm-12">
						<a itemprop="url" href="<?php echo bloginfo('url') ?>">
							<img itemprop="logo" src="<?php echo get_template_directory_uri(); ?>/img/logo.png">
						</a>
						<div class="footer-social">
							<a href="#"><i class="fab fa-facebook-f"></i></a>
							<a href="#"><i class="fab fa-twitter"></i></a>
							<a href="#"><i class="fab fa-linkedin"></i></a>
						</div>
	                </div>
				</div>
			</div>
			<div class="copy">
				&copy; <?php echo date("Y") . " " . get_bloginfo("name"); ?>
			</div>
		</footer>
		<?php wp_footer(); ?>
		<div class="mobile-menu" id="mob-menu" onclick="myFunction1(this)">
			<nav class="mobile-navigation" itemscope itemtype="http://schema.org/SiteNavigationElement"><?php
					wp_nav_menu([
						'theme_location' => 'primary-menu',
						'menu_class' => 'main-menu',
						'container' => '',
					]); ?>
			</nav>
		</div>
	</body>
</html>