<?php get_header(); ?>
<div class="single-post-content">
	<div class="right_block">
		<div class="flex-container image_buzz">
			<div class="buzz">
				<img src="../wp-content/themes/digi-theme/img/dance.jpg" alt="" title="">
				<h1 class="page-g-title">BUZZ & JAZZ</h1>
			</div>
		</div>
	</div>
	<div class="container">
	   <div class="row">
	       <div class="col-md-4 col-sm-12 boy-image">
		    	<img src="../wp-content/themes/digi-theme/img/boy.png" alt="" title=""> 
		   </div>
		   <div class="col-md-8 col-sm-12 thumb">
				<?php
					if (have_posts()) :
						while (have_posts()) :
							the_post(); ?>
							<?php the_post_thumbnail(); ?>
							<h1><?php the_title(); ?></h1>
							<?php
						endwhile;
					endif; ?>
		   </div>
		   <div class="col-md-10 col-sm-12 sing-content">
		   <?php
					if (have_posts()) :
						while (have_posts()) :
							the_post(); ?>
							<p><?php the_content(); ?></p><?php
						endwhile;
					endif; ?>
		   </div>
		</div>
	</div>
</div>
<?php get_footer(); ?>