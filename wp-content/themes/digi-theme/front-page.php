<?php 
/**
* Template Name: Homepage
*
* @package WordPress
* @subpackage Digilite Theme
*/

get_header(); ?>
<section class="hero-mobile">
   <div class="container">
      <h2 class="page-title"><?php the_field("homepage_title") ?></h2>
   </div>
</section>

<section id="owl-demo" class="hero owl-theme owl-carousel">
<?php

// Check rows exists.
if( have_rows('hero_section_background_slide') ):

    // Loop through rows.
    while( have_rows('hero_section_background_slide') ) : the_row();

        // Load sub field value.
        $slide = get_sub_field('home_slide_image'); ?>
        <div class="item"><img src="<?= $slide; ?>" alt=""></div>
   <?php // End loop.
    endwhile;

// No value.
else :
    // Do something...
endif; ?>
</section>
<section class="service">
	<div class="container">
		<h3 class="s-title"><?php the_field("service_title"); ?></h3>
		<div class="service-icon flex-container">
			<?php
			if( have_rows('list_services') ):
				while( have_rows('list_services') ) : the_row();
					$title = get_sub_field('one_service_title');
					$icon = get_sub_field('icon'); ?>
					<div class="col-md-4 col-sm-12">
					   <img src="<?= $icon; ?>" alt="" title="">
                       <a href="<?php the_sub_field("page_link"); ?>" class="service-name"><?= $title; ?></a>
					</div>
				<?php endwhile;
			else :
			endif;
			?>
        </div>
    </div>
</section>
<section class="brands cover" id="brands-container">
		<h5 class="block-name">Brands</h5>
		<div id="slide-control" class="col-md-12 nav-control d-md-flex justify-content-md-end"></div>
		<div class="slide-brands">
					
			    <?php
				if( have_rows('brands_slide') ):
					while( have_rows('brands_slide') ) : the_row();
					    $count = 1;
						$logo = get_sub_field('slide_logo');
						$bg = get_sub_field('slide_background'); ?>
						<div class="item item<?= $count; ?>" style="width: 100%; background-image: url(<?= $bg; ?>); min-height: 360px;">
                            <a href="<?php the_sub_field("br_page_link"); ?>" class="brh"><img src="<?= $logo; ?>" alt="" title="" /></a>
						</div>
						<?php $count++; ?>
					   <?php endwhile;
				else :
				endif;
				?>
			</div>
</section>
<section class="success text-center">
	<div class="container">
		<h5 class="block-name light-name">Success</h5>
		<h3 class="s-title"><?php the_field("sc_title"); ?></h3>
		<p class="block-desc"><?php the_field("sc_desc"); ?></p>
		<div class="count flex-container">
        <?php
				if( have_rows('sc_icon_list') ):
					while( have_rows('sc_icon_list') ) : the_row();
						$scicon = get_sub_field('sc_icon');
						$scnumber = get_sub_field('sc_number');
						$sctitle = get_sub_field("sc_in_title"); ?>
						<div class="col-md-4 col-sm-12">
							<img src="<?= $scicon; ?>" alt="" title="" />
							<p class="sc_title"><?= $sctitle; ?></p>
                        </div>
					   <?php endwhile;
				else :
				endif;
				?>
		</div>
	</div>
</section>
<section class="blog">
	<div class="container text-center">
	<h5 class="block-name light-name">Brands fifth ave</h5>
	<h3 class="s-title"><?php the_field("sc_title"); ?></h3>
		<div class="post-container flex-container">
		<?php
if( have_rows('posts') ):
    while( have_rows('posts') ) : the_row();
		$pimg = get_sub_field('p_image');
		$pauth = get_sub_field('p_author');
		$ptitle = get_sub_field('p_title');
		?>
            <div class="blog-item col-md-4 col-sm-12">
				<div class="thumb text-center">
					<img src="<?= $pimg; ?>" alt="">
				</div>
				<p class="post-author"><?= $pauth; ?></p>
				<p class="post-title"><?= $ptitle; ?></p>
			</div>
			<?php endwhile; else : endif; ?>
				
    	</div>
	</div>
</section>
<section class="consulting text-center">
	<div class="container">
		<h3 class="s-title"><?php the_field("cons_title"); ?></h3>
		<p class="block-desc"><?php the_field("cons_desc"); ?></p>
		<a href="mailto:info@brandsfifthave.com" class="book btn-b"><?php the_field("cons_button"); ?></a>
	</div>
</section>
<section class="full">
	<?php $fullimage = get_field("full_image"); ?>
	<img src="<?= $fullimage; ?>" alt="" title="">
</section>
<?php get_footer(); ?>